-module(sanity_test).

string_tokens ->
  [A | _] = string:tokens("this", "i"),	%% A = th
  [B | _] = string:tokens("this", "a"), %% B = this
  io:format("done\n").