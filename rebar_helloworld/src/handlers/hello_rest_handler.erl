-module(hello_rest_handler).

-export([init/3]).
-export([terminate/3]).
-export([rest_init/2]).
-export([rest_terminate/2]).

-export([allowed_methods/2]).

% init(_Type, Req, []) ->
init({tcp, http}, _Req, _Opts) ->
	{upgrade, protocol, cowboy_rest};
init(_Type, Req, _Opts) ->
	{ok, Req2} = cowboy_req:reply(
		500, [
			{<<"content-type">>, <<"text/plain">>}
		],
		"We don't serve non_rest here. Please use the root path.",
		Req),
	{shutdown, Req2, no_state}.

rest_init(Req, _Opts) ->
	{ok, Req2} = cowboy_req:reply(
		200, [
			{<<"content-type">>, <<"text/plain">>}
		],
		<<"Is this really the hello world on rest?">>,
		Req),
	{Host_url, _} = cowboy_req:host_url(Req),
	io:format("base url of the host server: ~s~n", [Host_url]),
	{ok, Req2, no_state}.

allowed_methods(Req, State) ->
	io:format("calls allowed method~n"),
	{[<<"GET">>, <<"HEAD">>, <<"OPTIONS">>], Req, State}.

rest_terminate(_Req, _State) -> ok.

terminate(_Reason, _Req, _State) -> ok.
