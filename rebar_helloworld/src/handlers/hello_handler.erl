-module(hello_handler).

-export([init/3]).
-export([handle/2]).
-export([terminate/3]).

% init(_Type, Req, []) ->
init({ssl, _}, Req, _Opts) ->
  {ok, Req2} = cowboy_req:reply(
    500, [
      {<<"content-type">>, <<"text/plain">>}
    ],
    "We don't serve ssl.",
    Req),
  {shutdown, Req2, no_state};
init(_Type, Req, _Opts) ->
  {ok, Req, no_state}.

handle(Req, State) ->
  {ok, Req2} = cowboy_req:reply(
    200, [
      {<<"content-type">>, <<"text/plain">>}
    ],
    <<"Is this really the hello world without rest?">>,
    Req),
  {Host_url, _} = cowboy_req:host_url(Req),
  io:format("base url of the host server: ~s", [Host_url]),
  {ok, Req2, State}.

terminate(_Reason, _Req, _State) ->
  ok.
