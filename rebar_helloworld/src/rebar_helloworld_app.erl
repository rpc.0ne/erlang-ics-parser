%%%-------------------------------------------------------------------
%% @doc rebar_helloworld public API
%% @end
%%%-------------------------------------------------------------------

-module(rebar_helloworld_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%%====================================================================
%% API
%%====================================================================

start(_StartType, _StartArgs) ->
	rebar_helloworld_sup:start_link(),
	io:format("Welcome to rebar_helloworld application.~n"),
	Dispatch = cowboy_router:compile([
		%% {HostMatch, list({PathMatch, Handler, Opts})}
		{'_', [
			{"/", hello_handler, []},
			{"/rest/", hello_rest_handler, []}
		]}
	]),
	cowboy:start_http(
		my_http_listener,
		100,
		[{ip, {0,0,0,0}}, {port, 8080}],
		[
			{env, [{dispatch, Dispatch}]},
			{max_keepalive, 100}
		]
	).

%%--------------------------------------------------------------------
stop(_State) ->
	io:format("Thank you for using rebar_helloworld application.~n"),
	ok.

%%====================================================================
%% Internal functions
%%====================================================================
