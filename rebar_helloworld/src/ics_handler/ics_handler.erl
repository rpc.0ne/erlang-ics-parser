-module(ics_handler).
-export([read_icalendar/1]).

read_icalendar(FileName) ->
  {ok, IODevice} = file:open(FileName, [read]),
  read_vcomponent(IODevice, #{}, no_state).
read_vcomponent(IODevice, Map, PrevKey) ->
  CurrVComponent = maps:get(vComponent, Map),
  case read_line(IODevice) of
    eof ->
      file:close(IODevice),
      ok; %% TODO return
    "BEGIN:" ++ NextVComponent ->
      %% CHECK this part is stateful. "IODevice" holds the state of line reading
      read_vcomponent(IODevice, #{vComponent => NextVComponent}, no_state),
      read_vcomponent(IODevice, Map, no_state);
    "END:" ++ CurrVComponent ->
      save_vcomponent(Map);
    "END:" ++ UnknownComponent ->
      %% CHECK return clause should be dropped silently. is "exit" the right way to do it?
      %%wait, should it be dropped silently? or not? the rest of file might be compromised too
      exit("Problem parsing: " ++ UnknownComponent);
    " " ++ AppendedLine ->
      UpdatedMap = Map#{PrevKey := (maps:get(PrevKey, Map) ++ AppendedLine)},
      read_vcomponent(IODevice, UpdatedMap, PrevKey);
    "" ->
      read_vcomponent(IODevice, Map, PrevKey);
    Line ->
      {ok, Key, Value} = parse_vproperties(Line),
      read_vcomponent(IODevice, Map#{Key => Value}, Key)
  end.
save_vcomponent(Map) ->
  ok. %% TODO impl
parse_vproperties(Line) ->
  %% TODO handle the other one with semicolon
  [Linehead | Linetail] = string:tokens(Line, ":"),
  CurrentKey = list_to_atom(string:to_lower(Linehead)),
  {ok, CurrentKey, hd(Linetail)}.
read_line(IODevice) ->
  io:get_line(IODevice, "").

parse_vcalendar(Device, Map) ->
  Line = io:get_line(Device, ""),
  case Line of
    "END:VCALENDAR" -> read_vcomponent(Device, Map, no_state);
    "BEGIN:VEVENT" -> parse_vevent(Device, Map);

    %% TODO not handled yet
    "BEGIN:VTODO" -> skip_parsing(Device, Map);
    "BEGIN:VJOURNAL" -> skip_parsing(Device, Map);
    "BEGIN:VFREEBUSY" -> skip_parsing(Device, Map);
    "BEGIN:VTIMEZONE" -> skip_parsing(Device, Map);
    "BEGIN:VALARM" -> skip_parsing(Device, Map);

    _ -> ok %% TODO parse thingies
  end.
parse_vevent(Device, Map) ->
  Line = io:get_line(Device, ""),
  case string:to_upper(Line) of
    "END:VEVENT" ->
      %% TODO save to storage
      parse_vcalendar(Device, Map);
    [$  | _] -> ok; %% TODO append previous thing
    _ -> ok %% TODO parse thingies
  end.
skip_parsing(Device, Map) ->
  Line = io:get_line(Device, ""),
  case Line of
    "END:VTODO" -> parse_vcalendar(Device, Map);
    "END:VJOURNAL" -> parse_vcalendar(Device, Map);
    "END:VFREEBUSY" -> parse_vcalendar(Device, Map);
    "END:VTIMEZONE" -> parse_vcalendar(Device, Map);
    "END:VALARM" -> parse_vcalendar(Device, Map);
    _ -> skip_parsing(Device, Map)
  end.

read_component(Device, Map, PrevLine) ->
  case read_line(Device) of
    " " ++ CurrLine -> read_component(Device, Map, PrevLine ++ CurrLine);
    Line ->
      case string:trim(PrevLine) of
        "BEGIN:VEVENT" -> ok;  %% TODO impl
        "BEGIN:" ++ SkippedComponent ->
          EndSkip = "END:" ++ string:uppercase(SkippedComponent),
          skip_until(Device, EndSkip);
        "END:VEVENT" -> ok;  %% TODO impl
        _ -> parse_vcomponent(PrevLine)
      end,
      read_component(Device, Map, Line)
  end.
parse_vcomponent(Property) ->
  ok.
skip_until(Device, UppercaseSearchedLine) ->
  case string:trim(string:uppercase(read_line(Device))) of
    UppercaseSearchedLine -> ok;          %% base
    _ -> skip_until(Device, UppercaseSearchedLine) %% recurrence
  end.

